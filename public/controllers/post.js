/* eslint-disable */
const app = document.app;

app.controller('PostController', ['$scope', '$http', function ($scope, $http) {
  $scope.lastId = 0;
  $scope.comments = [];
  $scope.init = function (id) {
    getComments(id);
    $scope.id = id;
  };
  $scope.getComments = function () {
    getComments($scope.id);
  };

  const getComments = function (id) {
    $http.get(`/post/${id}/comments/${$scope.lastId}`)
      .then(function(result) {
        console.log(result);
        result.data.comments.forEach((comment) => {
          $scope.comments.push(comment);
        });
        $scope.lastId = result.data.id;
      }, function(error) {
        console.log(error);
      });
  }
}]);
