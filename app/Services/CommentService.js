'use strict';

const CommentRepository = use('App/Repositories/CommentRepository');
const PostRepository = use('App/Repositories/PostRepository');
const ShopRepository = use('App/Repositories/ShopRepository');
const ShopUserRepository = use('App/Repositories/ShopUserRepository');
class CommentService {
  constructor () {
    this.cmtRepo = new CommentRepository();
    this.postRepo = new PostRepository();
    this.shopRepo = new ShopRepository();
    this.shopUserRepo = new ShopUserRepository();
  }

  /**
   * getComments
   * @param postId
   * @param lastId
   * @returns {Promise.<void>}
   */
  async getComments (postId, lastId) {
    if (+lastId === 0) {
      const lastComment = await this.cmtRepo.getLastId(postId);
      if (!lastComment) {
        return {
          comments: [],
          id: null,
        };
      }
      lastId = lastComment.id + 1;
    }
    const comments = await this.cmtRepo.getComments(postId, lastId);
    const post = await this.postRepo.getPost(postId);
    if (post.shop_id) {
      const employeeIds = [];
      const shop = await this.shopRepo.getShop(post.shop_id);
      const employee = await this.shopUserRepo.getEmployee(post.shop_id);
      employee.forEach((employ) => {
        employeeIds.push(employ.user_id);
      });
      comments.forEach((comment) => {
        if (comment.user_id === shop.owner_id) {
          comment.type = 0;
        } else if (employeeIds.indexOf(comment.user_id) >= 0) {
          comment.type = 1;
        } else {
          comment.type = 2;
        }
      });
    }
    const id = comments.length > 0 ? comments[comments.length - 1].id : null;
    return { comments, id };
  }
}

module.exports = CommentService;
