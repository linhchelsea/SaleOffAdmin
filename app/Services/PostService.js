'use strict';

const PostRepository = use('App/Repositories/PostRepository');
const { formatDate, formatDateTime } = use('App/Helpers');
class PostService {
  constructor () {
    this.postRepo = new PostRepository();
  }

  async getPostByUserId(userId) {
    const posts = await this.postRepo.getPostByUserId(userId);
    const acceptedPosts = [];
    const rejectedPosts = [];
    posts.forEach((post) => {
      post.start_date = formatDate(post.start_date);
      post.end_date = formatDate(post.end_date);
      if (post.is_trust) {
        acceptedPosts.push(post);
      } else {
        rejectedPosts.push(post);
      }
    });
    return { acceptedPosts, rejectedPosts };
  }
}

module.exports = PostService;
