'use strict';

const ShopUserRepository = use('App/Repositories/ShopUserRepository');
const UserRepository = use('App/Repositories/UserRepository');
class ShopUserService {
  constructor () {
    this.shopUserRepo = new ShopUserRepository();
    this.userRepo = new UserRepository();
  }

  /**
   * getEmployee
   * @param shopId
   * @returns {Promise.<*>}
   */
  async getEmployee(shopId) {
    const shopUsers = await this.shopUserRepo.getEmployee(shopId);
    const ids = [];
    shopUsers.forEach((user) => {
      ids.push(user.user_id);
    });
    const employee = await this.userRepo.getUsersByIds(ids);
    return employee;
  }
}

module.exports = ShopUserService;
