const moment = require('moment');

/**
 * formatTimestamp
 * @param date
 * @returns {string}
 */
const formatDate = date => moment(date).format('DD-MM-YYYY');
const formatDateTime = date => moment(date).format('HH:mm:ss DD-MM-YYYY');

module.exports = {
  formatDate,
  formatDateTime,
};

