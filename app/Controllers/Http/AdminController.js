'use strict';

const AdminRepository = use('App/Repositories/AdminRepository');
const RoleRepository = use('App/Repositories/RoleRepository');
const Config = use('Config');
const error = Config.get('error');
const success = Config.get('success');
class AdminController {
  constructor () {
    this.adminRepo = new AdminRepository();
    this.roleRepo = new RoleRepository();
  }

  /**
   * index
   * @param view
   * @returns {Promise.<String|*|Route>}
   */
  async index({ view }) {
    const admins = await this.adminRepo.getAdmins();
    return view.render('admin.index', { admins });
  }

  /**
   * add
   * @param view
   * @returns {Promise.<String|*|Route>}
   */
  async add ({ view }) {
    const roles = await this.roleRepo.getRoles();
    return view.render('admin.create', { roles });
  }

  /**
   * store
   * @param request
   * @param response
   * @param session
   * @returns {Promise.<Route|void|*>}
   */
  async store({ request, response, session }) {
    const { username, role, fullname } = request.only([
      'username',
      'role',
      'fullname',
    ]);
    const admin = await this.adminRepo.getAdminByUsername(username);
    if (admin) {
      session.withErrors({
        error: error.username_has_exist,
      });
      return response.route('admin.add');
    }
    const isStored = await this.adminRepo.store({ username, role, fullname });
    if (isStored) {
      session.flash({
        success: success.add_new_admin_success,
      });
    } else {
      session.withErrors({
        error: error.add_new_admin_fail,
      });
    }
    return response.route('admin.index');
  }

  /**
   * block
   * @param params
   * @param session
   * @param response
   * @returns {Promise.<Route|void|*>}
   */
  async block({ params, session, response }) {
    const { id } = params;
    const isBlock = await this.adminRepo.blockAdmin(id);
    if (isBlock) {
      session.flash({
        success: success.block_admin_success,
      });
    } else {
      session.withErrors({
        error: error.block_admin_fail,
      });
    }
    return response.route('admin.index');
  }

  /**
   * unblock
   * @param params
   * @param session
   * @param response
   * @returns {Promise.<Route|void|*>}
   */
  async unblock({ params, session, response }) {
    const { id } = params;
    const isBlock = await this.adminRepo.blockAdmin(id);
    if (isBlock) {
      session.flash({
        success: success.unblock_admin_success,
      });
    } else {
      session.withErrors({
        error: error.unblock_admin_fail,
      });
    }
    return response.route('admin.index');
  }
}

module.exports = AdminController;
