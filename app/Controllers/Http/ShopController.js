'use strict';

const ShopRepository = use('App/Repositories/ShopRepository');
const ShopUserRepository = use('App/Repositories/ShopUserRepository');
const DeleteShopRepository = use('App/Repositories/DeleteShopRepository');
const ShopUserService = use('App/Services/ShopUserService');
const { formatDate, formatDateTime } = use('App/Helpers');
const ApiRequest = use('App/Request/ApiRequest');
const Config = use('Config');
const error = Config.get('error');
const success = Config.get('success');
class ShopController {
  constructor () {
    this.shopRepo = new ShopRepository();
    this.shopUserService = new ShopUserService();
    this.shopUserRepo = new ShopUserRepository();
    this.deleteShopRepo = new DeleteShopRepository();
    this.apiRequest = new ApiRequest();
  }
  async index({ view }) {
    const shops = await this.shopRepo.getActivatedShops();
    return view.render('shop.index', { shops });
  }


  async waiting({ view }) {
    const shops = await this.shopRepo.getWaitingShops();
    return view.render('shop.waiting', { shops });
  }

  async requesting({ view }) {
    const shops = await this.shopRepo.getRequestingShops();
    return view.render('shop.requesting', { shops });
  }

  async detail ({ params, view }) {
    const shop = await this.shopRepo.getShopDetail(params.id);
    const employee = await this.shopUserService.getEmployee(params.id);
    shop.posts.forEach((post) => {
      post.start_date = formatDate(post.start_date);
      post.end_date = formatDate(post.end_date);
      post.created_at = formatDateTime(post.created_at);
    });
    return view.render('shop.detail', { shop, employee });
  }

  async accept({ params, auth, response, session }) {
    const adminId = +auth.user.id;
    const { id } = params;
    const url = `/accept/${id}/shop`;
    const body = await this.apiRequest.acceptAndReject({ url, adminId });
    if (body.error === 0) {
      session.flash({
        success: success.accept_shop_success,
      });

    } else {
      session.withErrors({
        error: error.accept_shop_fail,
      });
    }
    return response.redirect(`/shop/${id}/detail`);
  }

  async reject({ params, auth, response, session }) {
    const adminId = +auth.user.id;
    const { id } = params;
    const url = `/reject/${id}/shop`;
    const body = await this.apiRequest.acceptAndReject({ url, adminId });
    if (body.error === 0) {
      session.flash({
        success: success.reject_shop_success,
      });
    } else {
      session.withErrors({
        error: error.reject_shop_fail,
      });
    }
    return response.redirect(`/shop/${id}/detail`);
  }


  /**
   * postDeleteShop
   * @param params
   * @param session
   * @param response
   * @return {Promise.<void>}
   */
  async delete({ params, session, response }) {
    const { id } = params;
    const shop = await this.shopRepo.getShop(id);
    if (!shop) {
      session.withErrors({
        error: error.shop_not_found,
      });
      return response.redirect('back');
    }
    if (!shop.request_delete) {
      session.withErrors({
        error: error.not_request_delete_shop,
      });
      return response.redirect('back');
    }
    const shopUsers = await this.shopUserRepo.getEmployee(id);
    const ids = [];
    shopUsers.forEach((user) => {
      ids.push(user.user_id);
    });
    const isDelete = await this.deleteShopRepo.deleteShop(id);
    if (isDelete) {
      // call api send notification
      this.apiRequest.sendDeleteShopNotification({ shop, ids });
      session.flash({
        success: success.delete_shop_success,
      });
    } else {
      session.withErrors({
        error: error.delete_shop_fail,
      });
    }
    return response.redirect('shop.index');
  }
}

module.exports = ShopController;
