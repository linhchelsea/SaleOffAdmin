'use strict';

const UserRepository = use('App/Repositories/UserRepository');
const PostService = use('App/Services/PostService');
const { formatDate, formatDateTime } = use('App/Helpers');
class UserController {
  constructor () {
    this.userRepo = new UserRepository();
    this.postService = new PostService();
  }

  /**
   * index
   * @param view
   * @return {Promise.<*|Route|String>}
   */
  async index({ view }) {
    const users = await this.userRepo.getUsers();
    return view.render('user.index', { users });
  }

  async detail({ params, view }) {
    const { id } = params;
    const user = await this.userRepo.getUserById(id);
    user.birthday = formatDate(user.birthday);
    const posts = await this.postService.getPostByUserId(id);
    return view.render('user.detail', { user, posts })
  }
}

module.exports = UserController;
