'use strict';

const CommentService = use('App/Services/CommentService');
class CommentController {
  constructor () {
    this.commentService = new CommentService();
  }
  async getComments ({ params }) {
    const { postId, lastId } = params;
    const data = await this.commentService.getComments(postId, lastId);
    return data;
  }
}

module.exports = CommentController;
