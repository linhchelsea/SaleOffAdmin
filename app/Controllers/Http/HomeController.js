'use strict';

class HomeController {
  async getIndex ({ view }) {
    return view.render('home.index');
  }
}

module.exports = HomeController;
