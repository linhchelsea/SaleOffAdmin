'use strict';

const PostRepository = use('App/Repositories/PostRepository');
const ApiRequest = use('App/Request/ApiRequest');
const { formatDate } = use('App/Helpers');
const Config = use('Config');
const error = Config.get('error');
const success = Config.get('success');
class PostController {
  constructor () {
    this.postRepo = new PostRepository();
    this.apiRequest = new ApiRequest();
  }

  /**
   * getPostDetail
   * @param params
   * @param view
   * @return {Promise.<*|Route|String>}
   */
  async getPostDetail ({ params, view }) {
    const { id } = params;
    const post = await this.postRepo.getPostDetail(id);
    post.start_date = formatDate(post.start_date);
    post.end_date = formatDate(post.end_date);
    post.images = JSON.parse(post.images);
    return view.render('post.detail', { post });
  }

  async getPostWaiting ({ view }) {
    const posts = await this.postRepo.getPostWaiting();
    posts.forEach((post) => {
      post.start_date = formatDate(post.start_date);
      post.end_date = formatDate(post.end_date);
    });
    return view.render('post.waiting', { posts });
  }

  async accept({ params, auth, response, session }) {
    const adminId = +auth.user.id;
    const { id } = params;
    const url = `/accept/${id}/post`;
    const body = await this.apiRequest.acceptAndReject({ url, adminId });
    if (body.error === 0) {
      session.flash({
        success: success.accept_post_success,
      });
    } else {
      session.withErrors({
        error: error.accept_post_fail,
      });
    }
    return response.redirect(`/post/${id}/detail`);
  }

  async reject({ params, auth, response, session }) {
    const adminId = +auth.user.id;
    const { id } = params;
    const url = `/reject/${id}/post`;
    const body = await this.apiRequest.acceptAndReject({ url, adminId });
    if (body.error === 0) {
      session.flash({
        success: success.reject_post_success,
      });

    } else {
      session.withErrors({
        error: error.reject_post_fail,
      });
    }
    return response.redirect(`/post/${id}/detail`);
  }
}

module.exports = PostController;
