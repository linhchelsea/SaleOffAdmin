'use strict';

const Config = use('Config');
const error = Config.get('error');
class AuthController {
  /**
   * getLogin
   * @param view
   * @returns {String|*|Route}
   */
  getLogin ({ view }) {
    return view.render('auth.login');
  }

  /**
   * postLogin
   * @param request
   * @param session
   * @param response
   * @param auth
   * @returns {Promise.<*>}
   */
  async postLogin ({
    request, session, response, auth,
  }) {
    const { username, password } = request.only(['username', 'password']);
    try {
      await auth.attempt(username, password);
      if (auth.user.is_block) {
        await auth.logout();
        session.withErrors({
          error: error.your_account_is_blocked,
        });
        return response.route('auth.getLogin');
      }
      return response.route('index');
    } catch (err) {
      console.log(err);
      session.withErrors({
        error: error.email_password_not_correct,
      });
      return response.redirect('back');
    }
  }

  /**
   * getLogout
   * @param auth
   * @param response
   * @returns {Promise.<void|Route|*>}
   */
  async getLogout({ auth, response }) {
    await auth.logout();
    return response.route('auth.getLogin');
  }
}

module.exports = AuthController;
