'use strict';

const Model = use('Model');

class Shop extends Model {
  user () {
    return this.belongsTo('App/Models/User', 'owner_id', 'id');
  }

  category () {
    return this.belongsTo('App/Models/Category', 'shop_cat_id', 'id');
  }

  posts () {
    return this.hasMany('App/Models/Post');
  }
}

module.exports = Shop;
