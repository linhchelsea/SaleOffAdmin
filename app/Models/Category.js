'use strict';

const Model = use('Model');

class Category extends Model {
  shops () {
    return this.hasMany('App/Models/Shop');
  }
}

module.exports = Category;
