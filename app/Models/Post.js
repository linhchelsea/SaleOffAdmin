'use strict';

const Model = use('Model');

class Post extends Model {
  shop () {
    return this.belongsTo('App/Models/Shop');
  }
  product () {
    return this.belongsTo('App/Models/Product');
  }
  admin () {
    return this.belongsTo('App/Models/Admin');
  }
  user () {
    return this.belongsTo('App/Models/User');
  }
}

module.exports = Post;
