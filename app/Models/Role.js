'use strict';

const Model = use('Model');

class Role extends Model {
  admin () {
    return this.belongsToMany('App/Models/Admin');
  }
}

module.exports = Role;
