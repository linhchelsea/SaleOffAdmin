'use strict';

const Model = use('Model');

class User extends Model {
  shops () {
    return this.hasMany('App/Models/Shop');
  }
}

module.exports = User;
