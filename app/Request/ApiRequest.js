'use strict';

const request = require('request-promise');

const Env = use('Env');
class ApiRequest {
  acceptAndReject ({ url, adminId }) {
    const options = {
      method: 'POST',
      uri: `${Env.get('API_URL')}${url}`,
      body: {
        adminId,
      },
      json: true,
    };
    return request(options);
  }

  sendDeleteShopNotification ({ shop, ids }) {
    const options = {
      method: 'POST',
      uri: `${Env.get('API_URL')}/send-delete-shop_notification`,
      body: {
        shop,
        ids,
      },
      json: true,
    };
    return request(options);
  }

}

module.exports = ApiRequest;
