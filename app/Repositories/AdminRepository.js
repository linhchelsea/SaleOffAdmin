'use strict';

const Admin = use('App/Models/Admin');
const Env = use('Env');
class AdminRepository {
  /**
   * getAdmins
   * @returns {Promise.<Array>}
   */
  async getAdmins () {
    const admins = await Admin
      .query()
      .with('role')
      .whereNot('role_id', 1)
      .fetch();
    return admins ? admins.toJSON() : [];
  }

  /**
   * store
   * @param username
   * @param role
   * @param fullname
   * @returns {Promise.<boolean>}
   */
  async store({ username, role, fullname }) {
    try {
      await Admin.findOrCreate({
        username,
        full_name: fullname,
        role_id: role,
        password: Env.get('ADMIN_PASS'),
      });
      return true;
    } catch (err) {
      return false;
    }
  }

  /**
   * getAdminByUsername
   * @param username
   * @returns {Promise.<null>}
   */
  async getAdminByUsername(username) {
    const admin = await Admin.query().where('username', username).first();
    return admin ? admin.toJSON() : null;
  }

  /**
   * blockAdmin
   * @param id
   * @returns {Promise.<boolean>}
   */
  async blockAdmin(id) {
    try {
      const admin = await Admin.findOrFail(id);
      if (admin.role_id === 1) {
        return false;
      }
      admin.is_block = !admin.is_block;
      await admin.save();
      return true;
    } catch (err) {
      return false;
    }
  }
}

module.exports = AdminRepository;
