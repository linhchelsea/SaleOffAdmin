'use strict';

const Post = use('App/Models/Post');
class PostRepository {
  /**
   * getPostByUserId
   * @param userId
   * @return {Promise.<Array>}
   */
  async getPostByUserId(userId) {
    const posts = await Post
      .query()
      .with('admin', (builder) => {
        builder.select(['id', 'username']);
      })
      .with('product', (builder) => {
        builder.with('category');
      })
      .where('user_id', userId)
      .where('shop_id', null)
      .orderBy('id', 'desc')
      .fetch();
    return posts ? posts.toJSON() : [];
  }

  /**
   * getPostDetail
   * @param id
   * @return {Promise.<null>}
   */
  async getPostDetail(id) {
    const post = await Post
      .query()
      .where('id', id)
      .with('user')
      .with('admin', (builder) => {
        builder.select(['id', 'username']);
      })
      .with('product', (builder) => {
        builder.with('category');
      })
      .with('shop')
      .first();
    return post ? post.toJSON() : null;
  }

  /**
   * getPost
   * @param id
   * @returns {Promise.<null>}
   */
  async getPost(id) {
    const post = await Post.findOrFail(id);
    return post ? post.toJSON() : null;
  }

  /**
   * getPostWaiting
   * @returns {Promise.<Array>}
   */
  async getPostWaiting () {
    const posts = await Post
      .query()
      .with('user')
      .with('product', (builder) => {
        builder.with('category');
      })
      .where('shop_id', null)
      .where('is_trust', false)
      .where('admin_id', null)
      .orderBy('id', 'desc')
      .fetch();
    return posts ? posts.toJSON() : [];
  }
}

module.exports = PostRepository;
