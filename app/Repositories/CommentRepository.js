'use strict';

const Comment = use('App/Models/Comment');
const Config = use('Config');
const { pagination } = Config.get('global');
class CommentRepository {
  /**
   * getLastId
   * @param postId
   * @returns {Promise.<null>}
   */
  async getLastId (postId) {
    const comment = await Comment
      .query()
      .where('post_id', postId)
      .orderBy('id', 'desc')
      .first();
    return comment ? comment.toJSON() : null;
  }

  async getComments(postId, lastId) {
    const comments = await Comment
      .query()
      .with('user')
      .where('post_id', postId)
      .where('id', '<', lastId)
      .orderBy('id', 'desc')
      .limit(pagination)
      .fetch();
    return comments ? comments.toJSON() : [];
  }
}

module.exports = CommentRepository;
