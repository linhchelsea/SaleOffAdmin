'use strict';

const Shop = use('App/Models/Shop');
class ShopRepository {
  async getActivatedShops () {
    const shops = await Shop
      .query()
      .with('category')
      .with('user')
      .where('is_active', true)
      .orderBy('created_at', 'desc')
      .fetch();
    return shops.toJSON();
  }

  async getWaitingShops () {
    const shops = await Shop
      .query()
      .with('category')
      .with('user')
      .where('is_active', false)
      .orderBy('created_at', 'asc')
      .fetch();
    return shops.toJSON();
  }

  async getRequestingShops () {
    const shops = await Shop
      .query()
      .with('category')
      .with('user')
      .where('request_delete', true)
      .orderBy('updated_at', 'asc')
      .fetch();
    return shops.toJSON();
  }

  async getShopDetail(id) {
    const shop = await Shop
      .query()
      .with('category')
      .with('user')
      .with('posts')
      .where('id', id)
      .first();
    return shop.toJSON();
  }

  async getShop(id) {
    const shop = await Shop.find(id);
    return shop ? shop.toJSON() : null;
  }
}

module.exports = ShopRepository;
