'use strict';

const Role = use('App/Models/Role');
class RoleRepository {
  /**
   * getRoles
   * @returns {Promise.<void>}
   */
  async getRoles () {
    const roles = await Role
      .query()
      .whereNot('id', 1)
      .fetch();
    return roles.toJSON();
  }
}

module.exports = RoleRepository;
