'use strict';

const ShopUser = use('App/Models/ShopUser');

class ShopRepository {
  async getEmployee(shopId) {
    const employee = await ShopUser
      .query()
      .where('shop_id', shopId)
      .fetch();
    return employee ? employee.toJSON() : [];
  }
}

module.exports = ShopRepository;
