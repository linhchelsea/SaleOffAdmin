'use strict';

const Comment = use('App/Models/Comment');
const PostLike = use('App/Models/PostLike');
const Post = use('App/Models/Post');
const ShopFollow = use('App/Models/ShopFollow');
const ShopUser = use('App/Models/ShopUser');
const Shop = use('App/Models/Shop');
const Database = use('Database');
class DeleteShopRepository {
  constructor () {
    this.comment = Comment;
    this.postLike = PostLike;
    this.post = Post;
    this.shopFollow = ShopFollow;
    this.shopUser = ShopUser;
    this.shop = Shop;
  }

  /**
   * deleteShop
   * @param id
   * @return {Promise.<boolean>}
   */
  async deleteShop(id) {
    const db = await Database.beginTransaction();
    try {
      const shop = await this.shop.findOrFail(id);
      const posts = await this.post.query().where('shop_id', id);
      for (let i = 0; i < posts.length; i += 1) {
        // xoa comment
        await this.comment.query().where('post_id', posts[i].id).delete();
        // xoa like
        await this.postLike.query().where('post_id', posts[i].id).delete();
      }
      // xoa bai viet
      await this.post.query().where('shop_id', id).delete();
      // xoa follow
      await this.shopFollow.query().where('shop_id', id).delete();
      // xoa nhan vien
      await this.shopUser.query().where('shop_id', id).delete();
      // xoa shop
      await shop.delete();
      await db.commit();
      return true;
    } catch (err) {
      db.rollback();
      return false;
    }
  }
}

module.exports = DeleteShopRepository;
