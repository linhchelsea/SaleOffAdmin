'use strict';

const User = use('App/Models/User');
class UserRepository {
  /**
   * getUsers
   * @return {Promise.<void>}
   */
  async getUsers () {
    const users = await User.all();
    return users.toJSON();
  }

  async getUserById(id) {
    const user = await User.find(id);
    return user ? user.toJSON() : null;
  }


  async getUsersByIds(ids) {
    const users = await User
      .query()
      .whereIn('id', ids)
      .fetch();
    return users ? users.toJSON() : [];
  }
}

module.exports = UserRepository;
