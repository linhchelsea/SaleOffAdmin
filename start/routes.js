'use strict';

const Route = use('Route');

Route.get('/login', 'AuthController.getLogin')
  .as('auth.getLogin');
Route.post('/login', 'AuthController.postLogin')
  .as('auth.postLogin');
Route.group(() => {
  /**
   * ====================================================================
   *                               Logout                               *
   * ====================================================================
   */
  Route.get('/logout', 'AuthController.getLogout').as('auth.logout');
  Route.get('/', 'HomeController.getIndex').as('index');

  /**
   * ====================================================================
   *                                Admin                               *
   * ====================================================================
   */
  Route.get('/admin', 'AdminController.index').as('admin.index');
  Route.get('/admin/add', 'AdminController.add').as('admin.add');
  Route.get('/admin/:id/block', 'AdminController.block').as('admin.block');
  Route.get('/admin/:id/unblock', 'AdminController.unblock').as('admin.unblock');
  Route.post('/admin/store', 'AdminController.store').as('admin.store');

  /**
   * ====================================================================
   *                                Shop                                *
   * ====================================================================
   */
  Route.get('/shop', 'ShopController.index').as('shop.index');
  Route.get('/shop/waiting', 'ShopController.waiting').as('shop.waiting');
  Route.get('/shop/requesting', 'ShopController.requesting').as('shop.requesting');
  Route.get('/shop/:id/detail', 'ShopController.detail').as('shop.detail');
  Route.get('/shop/:id/accept', 'ShopController.accept').as('shop.accept');
  Route.get('/shop/:id/reject', 'ShopController.reject').as('shop.reject');
  Route.get('/shop/:id/delete', 'ShopController.delete').as('shop.delete');


  /**
   * ====================================================================
   *                                User                                *
   * ====================================================================
   */
  Route.get('/user', 'UserController.index').as('user.index');
  Route.get('/user/:id/detail', 'UserController.detail').as('user.detail');

  /**
   * ====================================================================
   *                                Post                                *
   * ====================================================================
   */
  Route.get('post/:id/detail', 'PostController.getPostDetail').as('post.detail');
  Route.get('post/waiting', 'PostController.getPostWaiting').as('post.waiting');
  Route.get('/post/:id/accept', 'PostController.ac  cept').as('post.accept');
  Route.get('/post/:id/reject', 'PostController.reject').as('post.reject');

  /**
   * ====================================================================
   *                               Comment                              *
   * ====================================================================
   */
  Route.get('/post/:postId/comments/:lastId', 'CommentController.getComments').as('comment.get');
}).middleware(['auth:session']);


Route.on('/').render('backend.modules.dashboard');
Route.on('/calendar').render('backend.modules.calendar');
Route.on('/chart').render('backend.modules.chart');
Route.on('/widget').render('backend.modules.widget');
Route.on('/timeline').render('backend.modules.timeline');
Route.on('/user').render('backend.modules.user');

// table
Route.on('/table-datatables-net').render('backend.tables.datatables-net');
Route.on('/table-extended').render('backend.tables.extended');
Route.on('/table-regular').render('backend.tables.regular');

// form
Route.on('/form-extended').render('backend.forms.extended');
Route.on('/form-regular').render('backend.forms.regular');
Route.on('/form-validation').render('backend.forms.validation');
Route.on('/form-wizard').render('backend.forms.wizard');

// component
Route.on('/buttons').render('backend.components.buttons');
Route.on('/grid').render('backend.components.grid');
Route.on('/icons').render('backend.components.icons');
Route.on('/notifications').render('backend.components.notifications');
Route.on('/sweet-alert').render('backend.components.sweet-alert');
Route.on('/panels').render('backend.components.panels');
Route.on('/typography').render('backend.components.typography');

// pages
Route.on('/lock').render('backend.pages.lock');
Route.on('/login').render('backend.pages.login');
Route.on('/pricing').render('backend.pages.pricing');
Route.on('/register').render('backend.pages.register');

// maps
Route.on('/maps/google').render('backend.maps.google');
Route.on('/maps/vector').render('backend.maps.vector');
