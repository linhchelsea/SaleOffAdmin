'use strict';

const Admin = use('App/Models/Admin');
const Env = use('Env');
class SuperAdminSeeder {
  async run () {
    await Admin.findOrCreate({
      username: Env.get('SA_USERNAME'),
      password: Env.get('SA_PASSWORD'),
      full_name: Env.get('SA_FULLNAME'),
      role_id: 1,
    });
  }
}

module.exports = SuperAdminSeeder;
