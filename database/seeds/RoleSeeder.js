'use strict';

const Role = use('App/Models/Role');
const names = ['Quản trị viên', 'Nhân viên'];
class RoleSeeder {
  async run () {
    for (let i = 0; i < names.length; i += 1) {
      await Role.findOrCreate({
        name: names[i],
      });
    }
  }
}

module.exports = RoleSeeder;
