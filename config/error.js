module.exports = {
  accept_shop_fail: {
    error: 1,
    message: 'Quá trình phê duyệt cửa hàng xảy ra sự cố!',
  },
  reject_shop_fail: {
    error: 2,
    message: 'Quá trình tạm ngưng hoạt động của cửa hàng xảy ra sự cố!',
  },
  reject_post_fail: {
    error: 3,
    message: 'Chặn bài viết không thành công!',
  },
  accept_post_fail: {
    error: 4,
    message: 'Duyệt bài viết không thành công!',
  },
  shop_not_found: {
    error: 5,
    message: 'Không tìm thấy cửa hàng!',
  },
  not_request_delete_shop: {
    error: 6,
    message: 'Cửa hàng này không có yêu cầu được xóa!',
  },
  delete_shop_fail: {
    error: 7,
    message: 'Xóa không thành công!',
  },
  add_new_admin_fail: {
    error: 8,
    message: 'Thêm nhân viên thành công!',
  },
  username_has_exist: {
    error: 9,
    message: 'Tên đăng nhập đã được sử dụng!',
  },
  your_account_is_blocked: {
    error: 10,
    message: 'Tài khoản của bạn đang bị tạm khóa!',
  },
  block_admin_fail: {
    error: 11,
    message: 'Khóa không thành công!',
  },
  unblock_admin_fail: {
    error: 12,
    message: 'Mở khóa không thành công!',
  },
};
