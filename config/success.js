module.exports = {
  accept_shop_success: {
    error: 1,
    message: 'Phê duyệt thành công!',
  },
  reject_shop_success: {
    error: 2,
    message: 'Đã tạm ngưng hoạt động của cửa hàng!',
  },
  reject_post_success: {
    error: 3,
    message: 'Chặn bài viết thành công!',
  },
  accept_post_success: {
    error: 4,
    message: 'Duyệt bài viết thành công!',
  },
  delete_shop_success: {
    error: 5,
    message: 'Xóa thành công!',
  },
  add_new_admin_success: {
    error: 6,
    message: 'Thêm nhân viên thành công!',
  },
  block_admin_success: {
    error: 7,
    message: 'Khóa thành công!',
  },
  unblock_admin_success: {
    error: 8,
    message: 'Mở khóa thành công!',
  },
};
